/* SPDX-License-Identifier: GPL-2.0-only */

/* omni-db-common.hh: Omni Notes DB, common types and functions
 *
 * Note: What belongs into this file instead of utils.hh?
 * - utils : truly general functionality, usable even in other projects
 * - omni-db-common : functionality common to this project, relating mostly to
 *                   omni-db module
 *
 * E.g. db_... datatypes are general but yet specific to omni-db only.
 * Therefore, it makes sennse to put them into module by its own.
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

#ifndef __OMNI_DB_COMMON_HH__
#define __OMNI_DB_COMMON_HH__

#include <string>

using db_text_t = std::string; // Java: String, SQL: TEXT
using db_timestamp_t = unsigned long long; // Java: Long, SQL: INTEGER
using db_category_id_t = unsigned long long; // Java: Long, SQL: INTEGER
using db_flag_t = unsigned int; // Java: Boolean, SQL: INTEGER
								// (1: true, 0: false)
using db_degree_t = long double; // Java: Double, SQL: REAL

// zero_elem: Define zero elements for above defined db_... types.
// (Note: Specialization of this are available in the .cc file).
template <typename BaseT> BaseT zero_val (void);

std::string
db_timestamp_to_string (db_timestamp_t t_ms, bool full_precision=true);

#endif //__OMNI_DB_COMMON_HH__

