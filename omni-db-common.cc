/* SPDX-License-Identifier: GPL-2.0-only */

/* omni-db.cc: Omni Notes DB classess (impl)
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

#ifndef LOG_LEVEL
#define LOG_LEVEL lINFO
#endif
#include "logcpp.hh"

#include "omni-db-common.hh"

template <> db_text_t      zero_val<db_text_t>      (void) { return ""; }
template <> db_timestamp_t zero_val<db_timestamp_t> (void) { return 0; }
template <> db_flag_t      zero_val<db_flag_t>      (void) { return 0; }
template <> db_degree_t    zero_val<db_degree_t>    (void) { return 0.0; }

/* db_timestamp_t is assumed to represent a milliseconds range duration since
 * Unix Epoch.
 *
 * Originally, a timestamp in a given Omni DB gets created using Java's
 * 'getTimeInMillis()' method (from Calendar), which returns a (Java) "long"
 * value. For the sake of simplicity, we will assume that this represents
 * milliseconds since Unix Epoch (00:00:00 01.01.1970 UTC), in all cases.
 *
 * The following implementation uses Howard Hinnant's date.h library to convert
 * a given timestamp value to string.  Using an optional boolean parameter, it
 * is possible to provide full precision (milliseconds), or a reduced precision
 * (days) only.
 *
 * Note: Starting with C++-20, parts of Howard's library are available in the
 * standard.
 */
#include "ext-libs/date.hh"
std::string
db_timestamp_to_string (db_timestamp_t t_ms, bool full_precision)
{
	// Make stringification available from Howard Hinnant's date.h library!
	using namespace date; 

	// Increasing time point precision to milliseconds
	using time_point = std::chrono::time_point<std::chrono::system_clock,
											   std::chrono::milliseconds>;

	// Initialize with zero time point and add given 't_ms' as a duration
	auto ts = time_point () + std::chrono::milliseconds(t_ms);

	std::ostringstream tmp;
	if (full_precision) {
		tmp << ts;
	}
	else {
		// Define 'days' since it's only available since C++-20
		using days = std::chrono::duration<uint64_t, std::ratio<86400>>;
		tmp << std::chrono::floor<days>(ts);		
	}
	return tmp.str();
}
