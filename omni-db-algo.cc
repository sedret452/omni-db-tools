/* SPDX-License-Identifier: GPL-2.0-only */

/* omni-db-algo.cc: Omni Notes DB algorithms (impl)
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

#include <algorithm>

#ifndef LOG_LEVEL
#define LOG_LEVEL lINFO
#endif
#include "logcpp.hh"

#include "omni-db-algo.hh"

// Slow, assuming unsorted set
// :TODO: Use set_intersect
// (see: https://en.cppreference.com/w/cpp/algorithm/set_intersection)
std::set<Note>
DB_Algo::intersect_nodes (const Omni_DB& db1, const Omni_DB& db2)
{
	// :TODO: Use an find function from set
	LOG(lDBG) << "intersect_nodes" << std::endl;
	
	std::set<Note>ret;
	for (const auto& n1: db1.notes_tab) {
		LOG(lDBG) << "Checking n1: " << n1.title << std::endl;
		for (const auto& n2: db2.notes_tab) {
			if (n1.is_same_as (n2)) {
				ret.insert (n1); // :TODO: Reduce copy overhead
			}
		}
	}
	return ret;
}

// :TODO: Something's fishy in the design when we have to forward to another
// function
std::set<Note>
DB_Algo::intersect_nodes (const DB_Access& db1, const DB_Access& db2)
{
	return intersect_nodes (db1.omni_db, db2.omni_db);
}

std::set<db_timestamp_t>
DB_Algo::intersect_ids (const DB_Access& db1
							 , const DB_Access& db2)
{
	std::set<db_timestamp_t> ret;
	for (auto e: intersect_nodes (db1.omni_db, db2.omni_db)) {
		ret.insert(e.creation);
	}
	return ret;
}

std::set<Note>
DB_Algo::disjoint_nodes (const Omni_DB& db1, const Omni_DB& db2)
{
	LOG(lDBG) << "disjoint_nodes" << std::endl;
	
	std::set<Note>ret;
	// (1) If N1 is not in N2
	for (const auto& n1: db1.notes_tab) {
		LOG(lDBG) << "Checking n1: " << n1.title << std::endl;
		bool found {false};
		for (const auto& n2: db2.notes_tab) {
			if (n1.is_same_as (n2)) {
				found = true;
				break;
			}
		}
		if (not found) {
			ret.insert(n1);
		}
	}
	// (2) If N2 is not in N1
	for (const auto& n2: db2.notes_tab) {
		LOG(lDBG) << "Checking n2: " << n2.title << std::endl;
		bool found {false};
		for (const auto& n1: db1.notes_tab) {
			if (n2.is_same_as (n1)) {
				found = true;
				break;
			}
		}
		if (not found) {
			ret.insert(n2);
		}
	}
	
	return ret;
}

// :TODO: Something's fishy in the design when we have to forward to another
// function
std::set<Note>
DB_Algo::disjoint_nodes (const DB_Access& db1, const DB_Access& db2)
{
	return disjoint_nodes (db1.omni_db, db2.omni_db);
}

std::set<db_timestamp_t>
DB_Algo::disjoint_ids (const DB_Access& db1
							 , const DB_Access& db2)
{
	std::set<db_timestamp_t> ret;
	for (auto e: disjoint_nodes (db1.omni_db, db2.omni_db)) {
		ret.insert(e.creation);
	}
	return ret;
}

bool
DB_Algo::check_fully_disjoint (const DB_Access& db1
									, const DB_Access& db2)
{
	auto intersect = intersect_ids (db1, db2);
	if (std::size (intersect) > 0) {
		return false;
	}
	else {
		return true;
	}
}

DB_Algo::Note_Ptr_Pair_L
DB_Algo::intersect_ptr_pairs (DB_Access& db1, DB_Access& db2)
{
	std::list<Note_Ptr_Pair> ret;

	using IT = std::set<Note>::iterator;

	for (IT it_1 {db1.omni_db.notes_tab.begin()}; it_1 != db1.omni_db.notes_tab.end(); it_1++) {
		for (IT it_2 {db2.omni_db.notes_tab.begin()}; it_2 != db2.omni_db.notes_tab.end(); it_2++) {
			if (it_1->is_same_as (*it_2)) {
				ret.push_back({Note_Ptr (&(*it_1)), Note_Ptr (&(*it_2))});
			}
		}
	}
	return ret;
}

void
DB_Algo::filter_out (DB_Algo::Note_Ptr_Pair_L& l
					 , DB_Algo::reduce_pred_t pred)
{
	l.erase(std::remove_if (l.begin(), l.end(), pred));
}

// For remove_copy_if
#include <execution>

DB_Algo::Note_Ptr_Pair_L
DB_Algo::filter_out_copy (DB_Algo::Note_Ptr_Pair_L& l
						  , DB_Algo::reduce_pred_t pred)
{
	Note_Ptr_Pair_L l_new;

	// :TODO: remove_copy_if returns erroneous l_new, I dunno what's happening
	// gcc-10 (Debian 10.2.1-6) 10.2.1 20210110:
	//
	// std::remove_copy_if (std::execution::seq, std::begin(l), std::end(l), l_new.begin(), pred);

	for (auto pair: l) {
		if (pred (pair)) {
			l_new.emplace_back (pair);
		}
	}

	return l_new;
}

void
DB_Algo::sync_inconsistent_longitude (DB_Algo::Note_Ptr_Pair_L& l)
{
	auto sync_longitude = [](Note_Ptr_Pair pair)
	{
		auto& field_1 {pair.first->longitude};
		auto& field_2 {pair.second->longitude};
						
		if (field_1 == 0.0 and field_2 != 0.0) {
			field_1 = field_2;
		}
		else if (field_1 != 0.0 and field_2 == 0.0) {
			field_2 = field_1;
		}
		else {
			LOG(lWARN) << "Longitude value is not inconsistently set!" << std::endl;
		}
	};

	std::for_each (l.begin(), l.end(), sync_longitude);	
}

void
DB_Algo::sync_inconsistent_latitude (DB_Algo::Note_Ptr_Pair_L& l)
{
	auto sync_latitude = [](Note_Ptr_Pair pair)
	{
		auto& field_1 {pair.first->latitude};
		auto& field_2 {pair.second->latitude};
						
		if (field_1 == 0.0 and field_2 != 0.0) {
			field_1 = field_2;
		}
		else if (field_1 != 0.0 and field_2 == 0.0) {
			field_2 = field_1;
		}
		else {
			LOG(lWARN) << "Latitude value is not inconsistently set!" << std::endl;
		}
	};

	std::for_each (l.begin(), l.end(), sync_latitude);	
}

// std::set<db_timestamp_t>
// DB_Algo::unset_base_metadata_ids (const DB_Access& db1
// 								  , const DB_Access& db2)
// {
// 	std::set<db_timestamp_t> ret;

// 	// :TODO:

// 	return ret;
// }
