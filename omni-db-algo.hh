/* SPDX-License-Identifier: GPL-2.0-only */

/* omni-db-algo.hh: Omni Notes DB algorithms
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

#ifndef __OMNI_DB_ALGO_HH__
#define __OMNI_DB_ALGO_HH__

#include <vector>
#include <set>
#include <memory>  // unique_ptr
#include <utility> // pair
#include <list>
#include <functional>
#include <cassert>

#include <iostream>

#include "omni-db.hh"

class DB_Algo {
public:

	static std::set<db_timestamp_t>
	intersect_ids (const DB_Access& db1, const DB_Access& db2);

	static std::set<Note>
	intersect_nodes (const DB_Access& db1, const DB_Access& db2);
	static std::set<Note>
	intersect_nodes (const Omni_DB& db1, const Omni_DB& db2);

	static std::set<Note>
	disjoint_nodes (const Omni_DB& db1, const Omni_DB& db2);
	static std::set<Note>
	disjoint_nodes (const DB_Access& db1, const DB_Access& db2);
	static std::set<db_timestamp_t>
	disjoint_ids (const DB_Access& db1, const DB_Access& db2);

	static bool
	check_fully_disjoint (const DB_Access& db1, const DB_Access& db2);

	using Note_Ptr = Note*;
	using Note_Ptr_Pair = std::pair<Note_Ptr, Note_Ptr>;
	using Note_Ptr_Pair_L = std::list<Note_Ptr_Pair>;

	// Get a list of intersection elments, as a pair of pointers to entries in
	// the original containers
	static Note_Ptr_Pair_L
	intersect_ptr_pairs (DB_Access& db1, DB_Access& db2);

	using reduce_pred_t = std::function<bool(Note_Ptr_Pair)>;

	// Filter out elements (pairs) from given list, using given predicate
	void filter_out (Note_Ptr_Pair_L&, reduce_pred_t predicate);
	
	static Note_Ptr_Pair_L
	filter_out_copy (Note_Ptr_Pair_L&, reduce_pred_t predicate);

	static Note_Ptr_Pair_L
	get_inconsistent_longitude (Note_Ptr_Pair_L& l);
	
	Note_Ptr_Pair_L
	get_inconsistent_latitude  (Note_Ptr_Pair_L& l);

	static Note_Ptr_Pair_L
	get_inconsistent_content (Note_Ptr_Pair_L& l);

	void sync_inconsistent_longitude (Note_Ptr_Pair_L& l);
	void sync_inconsistent_latitude  (Note_Ptr_Pair_L& l);
	
	// Old approach
	void reduce_to_unsynced_longitude (std::list<DB_Algo::Note_Ptr_Pair>& l);

    template<typename T, T Note::* mem_ptr>
	static bool
	is_inconsistently_set (const Note& left, const Note& right)
	{
		// Using variable to improve readability
		// (Note: this may decrease performance, though)
		const auto& zero      {zero_val<T>()};
		const auto& left_val  {left.*mem_ptr};
		const auto& right_val {right.*mem_ptr};

		bool ret = false;
		if ((left_val == zero and right_val != zero)
			or (left_val != zero and right_val == zero)) {
			ret = true;
		}
		return ret;
	}

	// Note: "_pair" allows to prevent overloading, ftm it simplifies auto
	// deduction
	template<typename T, T Note::* mem_ptr>
	static bool
	is_inconsistently_set_pair (Note_Ptr_Pair pair)
	{
		assert(pair.first != nullptr and pair.second != nullptr);
		const auto& n1 {*(pair.first)};
		const auto& n2 {*(pair.second)};
		
		return is_inconsistently_set<T, mem_ptr>(n1, n2);
	}

	// Reducing this to only one parameter didn't work. Although, this could be
	// possible, it may create a deeper problem due template instantiation.  I'm
	// relucutant to spend any more time on this.
	template<typename T, T Note::* mem_ptr> 
	static Note_Ptr_Pair_L
	get_inconsistent_field_ng (Note_Ptr_Pair_L& l)
	{
	    constexpr auto pred = is_inconsistently_set_pair <T, mem_ptr>;

		// Get a reduced list of inconsistently set longitude values
		return std::move(filter_out_copy(l, pred));
	}
	
};

#endif //__OMNI_DB_ALGO_HH__

