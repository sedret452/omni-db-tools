/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * inspect-omni-db.cc: Inspect "Omni Notes" (FOSS) Databases
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

extern "C" {
#include <unistd.h> // for getopt
#include <getopt.h> // for getopt long
}

#include <iostream>
#include <string>
#include <filesystem>
#include <vector>
#include <numeric> //iota
#include <cassert>

#ifndef LOG_LEVEL
#define LOG_LEVEL lINFO
#endif
#include "logcpp.hh"

#include "omni-db.hh"
#include "omni-db-algo.hh"

using std::cout, std::cerr, std::endl;
using std::string;
using std::map, std::vector;

const char* prog_name {"inspect-omni-db"};
const char* prog_version_str {"0.7"};

void
print_version ()
{	
	LOG(lSTD) << ::prog_name << ", version " << ::prog_version_str << endl;	
}

void
print_usage ()
{
	LOG(lSTD)
		<< ::prog_name << ", version " << ::prog_version_str << endl
		<< "Usage: " << endl
		<< "  " << ::prog_name << " [--help|--version] [MODE SYNTAX] " << endl
		<< endl
		<< "  General options" << endl
		<< "    -h, --help      Show this help" << endl
		<< "    -v, --version   Show version" << endl
		<< endl
		<< "  Read and Print database:" << endl
		<< "    " << ::prog_name << " -r <db-file>" << endl
		<< endl
		<< "  List intersecting elements in dbs:" << endl
		<< "    " << ::prog_name << " --list-intersect -D <in-db1> -D <in-db2>" << endl
		<< "    " << ::prog_name << " --list-intersect-chksum -D <in-db1> -D <in-db2>" << endl
		<< "  List intersecting elements with inconsistently set entries:" << endl
		<< "    " << ::prog_name << " --list-intersect-incons -D <in-db1> -D <in-db2>" << endl
		<< "  Delete notes from table, by id:" << endl
		<< "    " << ::prog_name << " --delete-ids id1,id2,...,idn -D <db>" << endl
		;
}

// Wrapping CLI error codes into an enumeration for readability.
// (note: Class enums turned out to be impractical for this, due to
//  additionally required value translation.)
enum cli_err {
	ok = 0, // No error, all is fine
	unknown_option = 1,
	multiple_modes_mixed = 2,
	too_many_outputs = 3,
	output_unset = 4,
	min_input_files = 5,
	not_exact_file_count = 6
};

void
merge_dummy (vector<string>&& input_filenames)
{
	map<string, DB_Access> input_dbs;
	for (auto&& filename: input_filenames) {
		auto&& dba {DB_Access(filename)};
		input_dbs.emplace (std::move(filename), std::move(dba));
	}
	for (auto& [name, db]: input_dbs) {
		db.read_notes_table();
		auto md5 {db.omni_db.checksum_notes_tab()};
		LOG(lINFO) << "db: <" << name << "> md5[" << md5 << "]" << endl;
	}
}

void
read_db_and_print_it (string read_db_name)
{
	DB_Access dba {read_db_name}; // may throw access error!
	dba.read_notes_table ();
	LOG(lINFO) << "notes table MD5: [" << dba.omni_db.checksum_notes_tab() << "]" << endl;
}

void
copy_db (string in_filename, string out_filename)
{
	auto in_file  {DB_File(in_filename)};

	// :TODO: Somehow, when using move in copy_to function it fucks up
	// things
	//auto out_file {DB_File(out_filename)};
	auto out_file {std::move(in_file.copy_to(out_filename))};

	// creates DBs and opens them already
	DB_Access in_dba  {std::move(in_file)};
	DB_Access out_dba {DB_Access(std::move(out_file))};
	{
		in_dba.read_notes_table();
		auto md5  {in_dba.omni_db.checksum_notes_tab()};
		LOG(lINFO) << "db (in): <" << in_filename << "> md5[" << md5 << "]" << endl;
	}

	{
		out_dba.read_notes_table();
		auto md5  {out_dba.omni_db.checksum_notes_tab()};
		LOG(lINFO) << "db (out): <" << out_filename << "> md5[" << md5 << "]" << endl;
	}
}

void
list_intersect (string filename_1, string filename_2)
{
	auto file_1 {DB_File(filename_1)};
	auto file_2 {DB_File(filename_2)};
	
	LOG(lINFO) << "List intersecting entries of databases:" << endl;
	LOG(lINFO) << " [1] <" << filename_1 << ">" << endl;
	LOG(lINFO) << " [2] <" << filename_2 << ">" << endl;

	DB_Access dba_1 {std::move(file_1)};
	dba_1.read_notes_table();
	DB_Access dba_2 {std::move(file_2)};
	dba_2.read_notes_table();

	for (const auto& e: DB_Algo::intersect_nodes (dba_1, dba_2)) {
		LOG(lSTD) << e.to_string(str_fmt::creation_date_title) << endl;
	}
}

void
list_intersect_with_chksums (string filename_1, string filename_2)
{
	auto file_1 {DB_File(filename_1)};
	auto file_2 {DB_File(filename_2)};
	
	LOG(lINFO) << "List intersecting entries (+ checksum) of databases:" << endl
			   << " [1] <" << filename_1 << ">" << endl
			   << " [2] <" << filename_2 << ">" << endl;

	DB_Access dba_1 {std::move(file_1)};
	dba_1.read_notes_table();
	DB_Access dba_2 {std::move(file_2)};
	dba_2.read_notes_table();

	for (auto cur_id: DB_Algo::intersect_ids(dba_1, dba_2)) {
		const auto* n1 {dba_1.omni_db.get_node_by_id(cur_id)};
		const auto* n2 {dba_2.omni_db.get_node_by_id(cur_id)};

		if (n1 != nullptr && n2 != nullptr) {
			auto sum_1  = n1->chksum_ign_last_modification();
			auto sum_2  = n2->chksum_ign_last_modification();
			if (sum_1 == sum_2) {
				cout << "Checksums are EQUAL: " << endl
					 << " " << "  " << n1->to_string(str_fmt::creation_date_title) << endl
					 << " " << "  [" << sum_1 << "] " << endl
					;
			}
			else {
				cout << "Checksums are NOT equal: " << endl
					 << " " << "1:" << n1->to_string(str_fmt::creation_date_title) << endl
					 << " " << "  [" << sum_1 << "] " << endl
					 << " " << "2:" << n2->to_string(str_fmt::creation_date_title) << endl
					 << " " << "  [" << sum_2 << "] " << endl
					;
				
			}
		}
	}
}

template <int N>
constexpr void
check_inconsistently_set_fields (std::array<bool, Note::fields_size>&a
										, const Note& left, const Note& right)
{
	static_assert(N >= 0 and N <= Note::fields_size,
				  "Provided integer parameter out of bounds!");

	constexpr Note::Fields field {static_cast<Note::Fields>(N)};
	using field_type = typename Note::Field_Type<field>;
	using value_type = typename field_type::value_type;		
	a[N] = DB_Algo::is_inconsistently_set<value_type, field_type::mem_ptr>(left, right);
	
	if constexpr (N > 0) {
		check_inconsistently_set_fields<N-1>(a, left, right);
	}
}

/* List only those intersecting elements of given DBs that are inconsistently
 * set regarding any field in Note.
 *
 * Note: for a pair of related notes, a field value is *inconsistently set*, if:
 * the value in one note is set and in the other one unset.
 */
void
list_intersect_with_inconsistency (string filename_1, string filename_2)
{
	// (1) Read the files and their tables
	auto file_1 {DB_File(filename_1)};
	auto file_2 {DB_File(filename_2)};
	LOG(lINFO) << "List all (inconsistently set) intersecting entries of databases:" << "\n"
			   << " [1] <" << filename_1 << ">" << "\n"
			   << " [2] <" << filename_2 << ">" << "\n";
	DB_Access dba_1 {std::move(file_1)};
	dba_1.read_notes_table();
	DB_Access dba_2 {std::move(file_2)};
	dba_2.read_notes_table();

	// (2) Create a list a list of Note Pairs that only holds elements that
	// represent the intersection between the two databases
	DB_Algo::Note_Ptr_Pair_L pair_list = DB_Algo::intersect_ptr_pairs(dba_1, dba_2);
	LOG(lINFO) << "Found " << pair_list.size()
			  << " candidates (intersecting entries)." << "\n";

	// (3) Create another list that filters out all intersecting elements that
	// are consistently set (note: there's no comparison of actual values but
	// only whether values are set/unset).
	DB_Algo::Note_Ptr_Pair_L pair_list_filtered;
	for (auto&& pair: pair_list) {
		LOG(lDBG) << "Checking pair: (" << (*(pair.first)) << "\n";

		// Doc Brown: "Nullptr? Where we are going we don't need nullptr!"		
		assert(pair.first != nullptr and pair.second != nullptr);
		auto& n1 {*(pair.first)};
		auto& n2 {*(pair.second)};
		
		constexpr size_t maxsize = Note::fields_size;
		constexpr size_t start = maxsize - 1;
		std::array<bool, maxsize> result_array;
		
		check_inconsistently_set_fields<start>(result_array, n1, n2);
		for (auto field: Note::fields_keys_array) {
			using key_t = std::underlying_type_t<Note::Fields>;
			key_t i {enum_val(field)};
			if (result_array[i]) {
				LOG(lSTD) << "field >" << Note::fields_names[i]
						  << "< inconsistently set, for: " << "\n"
						  << " " << filename_1 << ": " << "\n"
						  << "  " << n1.to_string(str_fmt::pretty) << "\n"
						  << " " << filename_2 << ": " << "\n"
						  << "  " << n2.to_string(str_fmt::pretty) << "\n"
					;
				
				// :TODO: Intersecting entries with more than one field
				// inconsistently set, would be added multiple times
				pair_list_filtered.emplace_back(std::move(pair));
			}
		}		
	}

	LOG(lINFO) << "Found " << pair_list_filtered.size()
			   << " intersecting entries that are inconsistently set." << "\n";
}

/* Note: Inconsistently set means, one is set and the other isn't (and vice
   versa)

   Note: Uses Note::Fields::title only for inspection.
*/
void
list_intersect_with_inconsistentcy_title (string filename_1, string filename_2)
{
	using field_type = Note::Field_Type<Note::Fields::title>;
	
	auto file_1 {DB_File(filename_1)};
	auto file_2 {DB_File(filename_2)};

	LOG(lINFO) << "List all (inconsistently set) intersecting entries of databases:" << endl
			   << " [1] <" << filename_1 << ">" << endl
			   << " [2] <" << filename_2 << ">" << endl;

	DB_Access dba_1 {std::move(file_1)};
	dba_1.read_notes_table();
	DB_Access dba_2 {std::move(file_2)};
	dba_2.read_notes_table();

	DB_Algo::Note_Ptr_Pair_L pair_list = DB_Algo::intersect_ptr_pairs(dba_1, dba_2);
	LOG(lINFO) << "Before removing all consistently set notes (title): size: " << pair_list.size() << "\n";

	using value_type = typename field_type::value_type;
	constexpr auto mem_p {field_type::mem_ptr};

	// for (DB_Algo::Note_Ptr_Pair pair : pair_list) {
	// 	DB_Algo::get_inconsistent_field_fn<value_type, mem_p>(pair);
	// }	

	auto pair_list_2 {DB_Algo::get_inconsistent_field_ng<value_type, mem_p> (pair_list)};

	LOG(lINFO) << "After removing all consistently set notes (title): size: " << pair_list_2.size() << "\n";
}

bool
parse_id_list (set<db_timestamp_t>& target_ids_vec, string id_list_str)
{
	// (Pre) Parse a string of expected syntax: >>id1,id2,id3,...<<
	std::stringstream tmp{id_list_str};
	string cur;
	// :TODO: Use split_string() in omni-db.cc instead
	while (getline(tmp, cur, ',')) {
		// (1) try to convert into db_timestamp_t
		// :TODO: Error handling
		std::size_t pos{};
		db_timestamp_t cur_id {std::stoull(cur, &pos)};

		// :TODO: (2) Add to target_ids_vec
		target_ids_vec.emplace (std::move(cur_id));
	}
	return true;
}

void
delete_note_ids_from (const string& db_filename
					  , const set<db_timestamp_t>& del_ids)
{
	DB_Access dba {db_filename};
	dba.delete_note_ids (del_ids);
}


int
main (int argc, char** argv)
{
	enum class cli_mode {
		read_print
		, list_intersect, list_intersect_chksum
		, list_intersect_with_inconsistency
		, delete_ids
		, unset_
	};
	cli_mode cli {cli_mode::unset_};
		
	string read_db_name {};
	vector<string> db_filenames {};
	set<db_timestamp_t> del_ids {};

	// Use compact names to reduce line length in opts definition
	#define ARG_N no_argument
	#define ARG_Y required_argument
	#define ARG_O optional_argument
	
	static char short_opts[] {"+hD:r:v"};
	static struct option long_opts[] = {
		{"help",                  ARG_N, 0, 'h'},
		{"delete-ids",            ARG_Y, 0, 'd'}, // short opt ignored
		{"list-intersect",        ARG_N, 0, 's'}, // short opt ignored
		{"list-intersect-chksum", ARG_N, 0, 'S'}, // short opt ignored
		{"list-intersect-incons", ARG_N, 0, 't'}, // short opt ignored
		{"read-db",  	          ARG_Y, 0, 'r'},
		{"version",  	          ARG_N, 0, 'v'},
		{0, 0, 0, 0}
	};
	::opterr = 0;		
	int opt_index {0};
	bool getopt_done {false};
	while (not getopt_done) {
		int curind = optind; // See: https://stackoverflow.com/a/2725408
		int c = getopt_long (argc, argv, short_opts, long_opts, &opt_index);
		switch (c) {
		    case -1:
				getopt_done = true;
				break;

			case 'h': // --help
				print_usage();
				exit (cli_err::ok);
				break;

			case 'd': // --delete-ids
				{
					if (cli == cli_mode::unset_) {
						cli = cli_mode::delete_ids;
						
						if (not parse_id_list (del_ids, string(optarg))) {
							;
							// :TODO: Error handling
						}
					}
					else {
						cerr << "Error: mode only selectable once (tried for --delete-ids)!" << endl;
						exit (cli_err::multiple_modes_mixed);
					}
				}
				break;

			case 'D': // -D
				db_filenames.emplace_back (string(optarg));
				break;

			case 'r': // --read-db
				{
					if (cli == cli_mode::unset_) {
						cli = cli_mode::read_print;
						read_db_name = string(optarg);
					}
					else {
						cerr << "Error: mode only selectable once (tried for -r)!" << endl;
						exit (cli_err::multiple_modes_mixed);
					}
				}
				break;

		    case 's': // --list-intersect
				{
					if (cli == cli_mode::unset_ || cli == cli_mode::list_intersect) {
						cli = cli_mode::list_intersect;
					}
					else {
						cerr << "Error: modes mixed!" << endl;
						exit (cli_err::multiple_modes_mixed);
					}
				}
				break;

		    case 'S': // --list-intersect-chksum
				{
					if (cli == cli_mode::unset_ || cli == cli_mode::list_intersect_chksum) {
						cli = cli_mode::list_intersect_chksum;
					}
					else {
						cerr << "Error: modes mixed!" << endl;
						exit (cli_err::multiple_modes_mixed);
					}
				}
				break;

		    case 't': // --list-intersect-incons
				{
					if (cli == cli_mode::unset_ || cli == cli_mode::list_intersect_chksum) {
						cli = cli_mode::list_intersect_with_inconsistency;
					}
					else {
						cerr << "Error: modes mixed!" << endl;
						exit (cli_err::multiple_modes_mixed);
					}
				}
				break;
				
			case 'v': // --version
				print_version();
				exit(cli_err::ok);
				break;

		    case '?': // Handling unknown options
				if (optopt) {
					if (isprint (optopt)) {
						cerr << "Unknown short option: -" << (char) optopt << endl;
					}
					else {
						cerr << "Unknown short option: UNPRINTABLE" << endl;
					}
				}
				else {
					cerr << "Unknown long option: " << argv[curind] << endl;
				}
				print_usage();
				exit (cli_err::unknown_option);
		}
	}

	LOG(lINFO) << ::prog_name << endl;
	switch (cli) {
	case cli_mode::read_print:
		// :TODO: error when read_db_name unset
		read_db_and_print_it (read_db_name);
		break;

	case cli_mode::list_intersect:
		if (db_filenames.size() == 2) {
			list_intersect (db_filenames[0], db_filenames[1]);
		}
		else {
			LOG(lERR) << "Wrong count of DB files for --list-intersect!" << endl;
			exit (cli_err::not_exact_file_count);
		}
		break;

	case cli_mode::list_intersect_chksum:
		if (db_filenames.size() == 2) {
			list_intersect_with_chksums (db_filenames[0], db_filenames[1]);
		}
		else {
			LOG(lERR) << "Wrong count of DB files for --list-intersect-chksum!" << endl;
			exit (cli_err::not_exact_file_count);
		}
		break;

	case cli_mode::list_intersect_with_inconsistency:
		if (db_filenames.size() == 2) {
			list_intersect_with_inconsistency (db_filenames[0], db_filenames[1]);
			// list_intersect_with_inconsistentcy_title (db_filenames[0], db_filenames[1]);
		}
		else {
			LOG(lERR) << "Wrong count of DB files for --list-intersect-chksum!" << endl;
			exit (cli_err::not_exact_file_count);
		}
		break;
		

	case cli_mode::delete_ids:
		if (db_filenames.size() == 1) {
			delete_note_ids_from (db_filenames[0], del_ids);
		}
		else {
			LOG(lERR) << "Only one DB file allowed for --delete-ids!" << endl;
			exit (cli_err::not_exact_file_count);
		}
		break;
	case cli_mode::unset_:
		LOG(lERR) << "no mode selected!" << endl;
		break;
	}

	return (cli_err::ok);
}
