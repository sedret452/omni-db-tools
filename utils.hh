/* SPDX-License-Identifier: GPL-2.0-only */

/* utils.hh: Helper utils
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

#ifndef __UTILS_HH__
#define __UTILS_HH__

#include <vector>
#include <string>

// Helper function: split a given string into slices, based on given
// delimiters (based on https://stackoverflow.com/a/600040).
//
// Note: Each slice will include the delimiter at the end
std::vector<std::string>
split_string_inclusive (const std::string& str, const std::string& delims);

// Calculate MD5 checksum of given text.
std::string
calc_md5 (std::string text);

#include <type_traits>
// Helper for enum class (see Scott Meyers book from 2015)
template<typename E>
constexpr auto
enum_val (E enumerator) noexcept
{
	return static_cast<std::underlying_type_t<E>>(enumerator);
}

#endif //__UTILS_HH__
