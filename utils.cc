#include "utils.hh"

using std::vector, std::string;

// Helper function: split a given string into slices, based on given
// delimiters (based on https://stackoverflow.com/a/600040).
// *Note: Each slice will include the delimiter at the end*
vector<string>
split_string_inclusive (const string& str, const string& delims)
{
	using pos_t = string::size_type;
	vector<string> slices{};

	// Initialize to positions of first slice
	pos_t slice_start = str.find_first_not_of(delims, 0);
	pos_t slice_end   = str.find_first_of    (delims, slice_start);

	while (string::npos != slice_end || string::npos != slice_start) {
		// Add current slice to the vector
		// (Note `(...) + 1` will include a single char delimiter)
		slices.emplace_back(str.substr(slice_start, (slice_end - slice_start) + 1));
		// Set positions of the next slice
		slice_start = str.find_first_not_of(delims, slice_end);
		slice_end = str.find_first_of(delims, slice_start);
	}
	return slices;
}


extern "C" {
#include <openssl/md5.h>
}
#include <sstream>
#include <iomanip>
using std::ostringstream;

// Calculate MD5 checksum of given text, via `libcrypto`
string
calc_md5 (string text)
{
	unsigned char digest[MD5_DIGEST_LENGTH];
	MD5_CTX context;
	MD5_Init(&context);
	MD5_Update(&context, text.c_str(), text.length()); 
	MD5_Final(digest, &context);

	ostringstream sout;
	sout<<std::hex<<std::setfill('0');
	for(long long c: digest) {
		sout<<std::setw(2)<<(long long)c;
	}
	return sout.str();
}
