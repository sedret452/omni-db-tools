/* SPDX-License-Identifier: GPL-2.0-only */

/* omni-db.cc: Omni Notes DB classess (impl)
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

#ifndef LOG_LEVEL
#define LOG_LEVEL lINFO
#endif
#include "logcpp.hh"

#include "omni-db.hh"

#include <filesystem>
#include <iostream> 
#include <vector>
#include <sstream>

using std::ostream, std::endl;
using std::ostringstream, std::string;
using std::map, std::vector;

const std::array<std::string, Note::fields_size> Note::fields_names {
	"creation", // Primary Key
	"last_modification",
	"title",
	"content",
	"archived",
	"trashed",
	"alarm",
	"reminder_fired",
	"recurrence_rule",
	"latitude",
	"longitude",
	"address",
	"category_id",
	"locked",
	"checklist",
};


// Get checksum as a string.
// Note: checksum_ used as a proxy field.
string
Note::checksum (void)
{
	if (checksum_.empty()) {
		checksum_ = calc_md5(this->to_string(str_fmt::raw));
	}
	return checksum_;
}

// Get an MD5 checksum of core data
// (creation, title, content, checklist flag)
string
Note::chksum_core_data (void) const
{
	ostringstream tmp;
	tmp << creation
		// << last_modification
		<< title
		<< content
		// << archived
		// << trashed
		// << alarm
		// << reminder_fired
		// << recurrence_rule
		// << latitude
		// << longitude
		// << address
		// << category_id
		// << locked
		<< checklist
		;
	return calc_md5 (tmp.str());
}

// Get an MD5 checksum of base metadata
// (alarm, recurrence_rule, latitude, longitude, address, category_id)
// Note: arguably "modification" is not part here
string
Note::chksum_base_metadata (void) const
{
	ostringstream tmp;
	tmp
		// << creation
		// << last_modification
		// << title
		// << content
		// << archived
		// << trashed
		<< alarm
		// << reminder_fired
		<< recurrence_rule
		<< latitude
		<< longitude
		<< address
		<< category_id
		// << locked
		// << checklist
		;
	return calc_md5 (tmp.str());
}

// Get an MD5 checksum of all metadata
// ("base metadata" + last_modification, archived, trashed, reminder_fired,
// locked)
string
Note::chksum_full_metadata (void) const
{
	ostringstream tmp;
	tmp
		// << creation
		<< last_modification
		// << title
		// << content
		<< archived
		<< trashed
		<< alarm
		<< reminder_fired
		<< recurrence_rule
		<< latitude
		<< longitude
		<< address
		<< category_id
		<< locked
		// << checklist
		;
	return calc_md5 (tmp.str());
}

// Get an MD5 checksum but ignore last_modification
string
Note::chksum_ign_last_modification (void) const
{
	ostringstream tmp;
	tmp
		<< creation
		// << last_modification
		<< title
		<< content
		<< archived
		<< trashed
		<< alarm
		<< reminder_fired
		<< recurrence_rule
		<< latitude
		<< longitude
		<< address
		<< category_id
		<< locked
		<< checklist
		;
	return calc_md5 (tmp.str());
}


// A note is the same as another note, if they have the same main ID, which is
// creation (time).
bool
Note::is_same_as (const Note& note) const
{
	return note.creation == creation;
}

bool
Note::equals_id (const db_timestamp_t& id) const
{
	return id == creation;
}


// Equality based on all fields of a given node
bool
Note::is_equal_all (const Note& note)
{
	// :TODO: Change this to functional style fold
	for (bool field_is_equal: has_equal_data(note)) {
		if (not field_is_equal) {
			return false;
		}
	}
	return true;
}

bool
Note::is_equal_ign_last_modification (const Note& other)
{
	// Overall strategy: fields are numerically ordered, hence let's compare
	// fields one-by-one, but skip last_modification field.
	// (Note: it is also assumed that underlying data type of enum is 'int')
	
	if (not has_equal_data(other, Fields::creation)) {
		return false;
	}
	int start_field = enum_val(Fields::title);
	int last_field  = enum_val(Fields::checklist);
	for (int cur = start_field; cur <= last_field; cur++) {
		Fields f {static_cast<Fields>(cur)};
		if (not has_equal_data (other, f)) {
			return false;
		}
	}
	return true;
}


Note::Fields_Diff
Note::has_equal_data (const Note& note)
{
	Note::Fields_Diff diff;
	for (int cur_field = 0; cur_field < fields_size; cur_field++) {
		Fields f {static_cast<Fields>(cur_field)}; // TODO: cast isn't safe here
		diff[cur_field] = has_equal_data (note, f);
	}
	return diff;
}


bool
Note::has_equal_data (const Note& note, const Fields f)
{
	switch (enum_val(f)) {
	case enum_val(Fields::creation):
		return note.creation == creation;
		break;
	case enum_val(Fields::last_modification):
		return note.last_modification == last_modification;
		break;
	case enum_val(Fields::title):
		return note.title == title;
		break;
	case enum_val(Fields::content):
		return note.content == content;
		break;
	case enum_val(Fields::archived):
		return note.archived == archived;
		break;
	case enum_val(Fields::trashed):
		return note.trashed == trashed;
		break;
	case enum_val(Fields::alarm):
		return note.alarm == alarm;
		break;
	case enum_val(Fields::reminder_fired):
		return note.reminder_fired == reminder_fired;
		break;
	case enum_val(Fields::recurrence_rule):
		return note.recurrence_rule == recurrence_rule;
		break;
	case enum_val(Fields::latitude):
		return note.latitude == latitude;
		break;
	case enum_val(Fields::longitude):
		return note.longitude == longitude;
		break;
	case enum_val(Fields::address):
		return note.address == address;
		break;
	case enum_val(Fields::category_id):
		return note.category_id == category_id;
		break;
	case enum_val(Fields::locked):
		return note.locked == locked;
		break;
	case enum_val(Fields::checklist):
		return note.checklist == checklist;
		break;
	}
	
	return false;
}


string
Note::to_string (str_fmt fmt) const
{
	ostringstream ret;
	switch (fmt) {
	case str_fmt::pretty:
		ret << "{"
			<< creation << "|"
			// << last_modification
			<< title
			// << endl << "<<<BEGIN>>>" << endl << content << endl << "<<<END>>>" << endl
			// << archived
			// << trashed
			// << alarm
			// << reminder_fired
			// << recurrence_rule
			// << latitude
			// << longitude
			// << address
			// << category_id
			// << locked
			// << checklist
			<< "}"
			<< " [" << checksum_ << "]"
			;
		break;
	case str_fmt::detailed:
		ret << endl << "{"
			<< endl << "  " << creation
			<< endl << "  " << last_modification
			<< endl << "  " << title
			<< endl << "  <<<BEGIN>>>"
			<< endl << content
			<< endl << "  <<<END>>>"
			<< endl << "  " << archived
			<< endl << "  " << trashed
			<< endl << "  " << alarm
			<< endl << "  " << reminder_fired
			<< endl << "  " << recurrence_rule
			<< endl << "  " << latitude
			<< endl << "  " << longitude
			<< endl << "  " << address
			<< endl << "  " << category_id
			<< endl << "  " << locked
			<< endl << "  " << checklist
			<< endl << "}" << " [" << checksum_ << "]"
			;
		break;
	case str_fmt::raw:
		ret << creation
			<< last_modification
			<< title
			<< content
			<< archived
			<< trashed
			<< alarm
			<< reminder_fired
			<< recurrence_rule
			<< latitude
			<< longitude
			<< address
			<< category_id
			<< locked
			<< checklist
			;
		break;

	case str_fmt::id_title:
		ret << "{"
			<< "ID: " << creation << " "
			// << last_modification
			<< "Title: " << title
			// << endl << "<<<BEGIN>>>" << endl << content << endl << "<<<END>>>" << endl
			// << archived
			// << trashed
			// << alarm
			// << reminder_fired
			// << recurrence_rule
			// << latitude
			// << longitude
			// << address
			// << category_id
			// << locked
			// << checklist
			<< "}"
			// << " [" << checksum_ << "]"
			;
		break;

	case str_fmt::creation_date_title:
		ret << "{"
			<< "Created: " << db_timestamp_to_string(creation, false)
			<< "|Title: " << title
			<< "|ID: " << creation
			<< "}"
			;
		break;
		
	}
	return {ret.str()};
}

bool operator< (const Note& l, const Note& r)
{
	return (l.creation < r.creation);
}

// :TODO: Select to_string(str_fmt::detailed) when DBG log level is
// selected
// See: https://stackoverflow.com/questions/23711389/template-with-bool-parameter#23711458
// for possible approaches
ostream& operator<< (ostream& strm, const Note& note)
{
	// strm << note.to_string(str_fmt::detailed) << endl;
	strm << note.to_string();
	return strm;
}

Note::Note(map<int, string> values)
{
	using NF = Note::Fields;

	// Some conversion lambdas for convenience
	auto val_text = [&values](Note::Fields f)
	{
		return values[enum_val(f)];
	};
	auto val_timestamp = [&values](Note::Fields f)
	{
		std::size_t pos{};
		string& s {values[enum_val(f)]};
		if (s.empty())
			return 0ull;
		else
			return std::stoull(s, &pos);
	};
	auto val_flag = [&values](Note::Fields f)
	{
		std::size_t pos{};
		string& s {values[enum_val(f)]};
		if (s.empty())
			return 0u;
		else
			return static_cast<unsigned int>(std::stoul(s, &pos));
	};
	auto val_degree = [&values](Note::Fields f)
	{
		char* end {nullptr};
		return std::strtold(values[enum_val(f)].c_str(), &end);
	};
	
	creation = val_timestamp(NF::creation);
	last_modification = val_timestamp(NF::last_modification);
	title = val_text(NF::title);
	content = val_text(NF::content);
	archived = val_flag(NF::archived);
	trashed = val_flag(NF::trashed);
	alarm = val_timestamp(NF::alarm);// :TODO: What to do on empty?
	reminder_fired = val_timestamp(NF::reminder_fired);
	recurrence_rule = val_text(NF::recurrence_rule);
	latitude = val_degree(NF::latitude);
	longitude = val_degree(NF::longitude);
	address = val_text(NF::address);
	category_id = val_timestamp(NF::category_id);// :TODO: What to do on empty?
	locked = val_flag(NF::locked);
	checklist = val_flag(NF::checklist);

	checksum();
}

string
Omni_DB::notes_tab_to_string () const
{
	ostringstream ret;
	for (const auto& note: notes_tab) {
		ret << note.to_string(str_fmt::raw);
	}
	return {ret.str()};
}

string
Omni_DB::checksum_notes_tab () const
{
	string text {notes_tab_to_string()};
	if (not text.empty()) {
		return calc_md5(text);
	}
	else {
		return {};
	}
}

size_t
Omni_DB::size_notes_tab () const
{
	return notes_tab.size();
}


DB_Access::DB_Access (string filename)
	: db_file_ {filename}
{
	if (not open_db_file()) {
		throw DB_Access_Err();
	}
}

DB_Access::DB_Access (DB_File&& db_file)
	: db_file_ {std::move(db_file)}
{
	if (not open_db_file()) {
		throw DB_Access_Err();
	}
}

Omni_DB::Omni_DB ()
	: notes_tab {}
{}


Omni_DB::Omni_DB (Omni_DB&& old)
	: version {std::move(old.version)}, notes_tab {std::move(old.notes_tab)}
{}

Omni_DB& Omni_DB::operator= (Omni_DB&& old)
{
	version = std::move(old.version);
	notes_tab = std::move(old.notes_tab);
	return *this;
}

DB_File::DB_File (string filename)
	: filename {std::move(filename)}
{}

// :TODO: For now, assuming that the user is the owner of the file but
// this needs to be checked properly in the future.
bool
DB_File::is_accessible ()
{
	namespace fs = std::filesystem;
	fs::path filepath {filename};
	if (not is_regular_file (filepath)) {
		return false;
	}
	const fs::perms fileperm {status(filepath).permissions()};
	if ((fs::perms::owner_read & fileperm) == fs::perms::none) {
		return false;
	}
	return true;
}

DB_File
DB_File::copy_to (string to_filename)
{
	namespace fs = std::filesystem;
	
	DB_File db {to_filename};
	fs::path source = filename;
	fs::path target = db.filename;

	// May throw fs::filesystem_error or even std::bad_alloc
	fs::copy_file(source, target, fs::copy_options::overwrite_existing);
	
	return db;
}

bool
DB_File::remove ()
{
	namespace fs = std::filesystem;
	fs::path filepath {filename};
	std::error_code ec;

	if (not is_regular_file (filepath)) {
		return false;
	}
	const fs::perms fileperm {status(filepath).permissions()};
	if ((fs::perms::owner_write & fileperm) == fs::perms::none) {
		return false;
	}
	fs::remove (filepath, ec);
	if (ec) {
		LOG(lERR) << "Couldn't delete DB file: " << ec.message() << endl;
		return false;		
	}
	return true;
}


DB_Access&
DB_Access::operator= (DB_Access&& old) {
	db_file_ = std::move (old.db_file_);
	db_handle_ = std::move (old.db_handle_);
	old.db_handle_ = nullptr;
	omni_db = std::move (old.omni_db);
	return *this;
}

DB_Access::DB_Access (DB_Access&& old)
	: db_file_ {std::move (old.db_file_)}, db_handle_ {std::move (old.db_handle_)}, omni_db {std::move (old.omni_db)}
{
	old.db_handle_ = nullptr;
}

DB_Access::~DB_Access ()
{
	close_db_file();
}

void
DB_Access::close_db_file ()
{
	if (db_handle_ != nullptr) {
		sqlite3_close(db_handle_);
		db_handle_ = nullptr;
	}
}

bool
DB_Access::close_and_remove_db_file ()
{
	close_db_file();
	return db_file_.remove ();
}

bool
DB_Access::open_db_file ()
{
	sqlite3* DB;
	const string& filename {db_file_.filename};
	int exit {sqlite3_open_v2(filename.c_str(), &DB, SQLITE_OPEN_READWRITE, NULL)};
	if (exit) {
		LOG(lERR) << "ERROR: Couldn't open database <" << filename << endl
				  << "\t" << sqlite3_errmsg(DB) << endl;
		return false;
	}
	else {
		db_handle_ = DB;
		return true;
	}
}

#include <cstdio>
// :TODO: Rewrite for LOGGER output
static int
debug_notes_table_callback (void* data, int num_columns, char** row_fields, char** col_names)
{
	fprintf(stderr, "%s: ", (const char*)data);
	for (int i = 0; i < num_columns; i++) {
		printf("%s = %s\n", col_names[i], row_fields[i] ? row_fields[i] : "NULL");
	}
	printf("\n");
	return 0;
}

bool
DB_Access::debug_notes_table ()
{
	// :TODO: Assumes db is openend correctly already

	string cmd {"SELECT * FROM notes;"};	
	auto callback = static_cast<sqlite3_callback>(debug_notes_table_callback);	

	string callback_line {"CALLBACK"};
	int rc = sqlite3_exec(db_handle_, cmd.c_str(), callback, (void*)callback_line.c_str(), NULL);
	if (rc != SQLITE_OK) {
		LOG(lERR) << "ERROR: reading notes table failed!" << endl;
		return false;
	}
	else {
		LOG(lINFO) << "SUCCESS: Reading notes table." << endl;
		return true;
	}
}

static int
callback_read_notes (void* data, int num_columns, char** row_fields, char** col_names)
{
	DB_Access* db_access = reinterpret_cast<DB_Access*>(data);
	map<int, string> values {};
	for (int i = 0; i < num_columns; i++) {
		if (row_fields[i]) {
			values[i] = string(row_fields[i]);
		}
		else {
			values[i] = string("");
		}
	}
	Note note = Note (values);
	Omni_DB& omni_db {db_access->omni_db};
	omni_db.notes_tab.insert(note);
	LOG(lDBG) << "read callback: " << note << endl;
	return 0;
}

bool
DB_Access::read_notes_table ()
{
	// :TODO: Assumes db is openend correctly already

	string cmd {"SELECT * FROM notes;"};
	auto callback = static_cast<sqlite3_callback>(callback_read_notes);

	int rc = sqlite3_exec(db_handle_, cmd.c_str(), callback, (void*)this, NULL);
	if (rc != SQLITE_OK) {
		LOG(lERR) << "reading notes table failed!" << endl;
		return false;
	}
	else {
		LOG(lINFO) << "SUCCESS: Reading notes table." << endl;
		return true;
	}
}

// :TODO: Rewrite for LOGGER output
// :TODO: For actual merge (using attach strategy) this woulnd't print out anything
static int
callback_merge_notes (void* data, int num_columns, char** row_fields, char** col_names)
{
	fprintf(stderr, "%s: ", (const char*)data);
	for (int i = 0; i < num_columns; i++) {
		printf("%s = %s\n", col_names[i], row_fields[i] ? row_fields[i] : "NULL");
	}
	printf("\n");
	return 0;
}

bool
DB_Access::merge_notes_table (string from_db_name)
{
	// :TODO: Assumes db is openend correctly already

	bool ret {true};

	// Generate SQL commands via stringstream for convenience
	ostringstream tmp;
	tmp << "attach '" << from_db_name << "' as _merged" << ";"
		<< "begin" << ";"
		<< "insert into notes select * from _merged.notes" << ";"
		<< "end" << ";"
		// << "commit" << ";"
		<< "detach _merged" << ";";
	vector<string> cmds {split_string_inclusive (tmp.str(), ";")};

	// Call each command individually (this helps debugging problems)
	for (auto cmd: cmds) {
		string callback_line {"CALLBACK (" + cmd + ")"};
		auto callback = static_cast<sqlite3_callback>(callback_merge_notes);
		int rc = sqlite3_exec(db_handle_, cmd.c_str(), callback, (void*)callback_line.c_str(), NULL);
		if (rc != SQLITE_OK) {
			LOG(lERR) << callback_line << " failed!" << "[" << std::to_string(rc) << "]" << endl;
			ret = false;
		}
		else {
			LOG(lINFO) << "SUCCESS: " << callback_line << " !" << endl;
		}
	}
	return ret;
}

// Omni_DB::Note_Const_It
// Omni_DB::get_node_by_id_ng(db_timestamp_t id) const
// {
// 	for (Note_Const_It it {notes_tab.cbegin()}; it != notes_tab.cend(); it++) {
// 		if (it->equals_id (id)) {
// 			return it;
// 		}
// 	}
// 	//return notes_tab.cend();
// 	return std::cend(notes_tab);
// }
	

//std::set<Note>::const_iterator
const Note*
Omni_DB::get_node_by_id(db_timestamp_t id) const
{
	using it_t = std::set<Note>::const_iterator;
	for (it_t it {notes_tab.cbegin()}; it != notes_tab.cend(); it++) {
		if (it->equals_id (id)) {
			return &(*it);
		}
	}
	//return notes_tab.cend();
	return nullptr;
}

static int
callback_delete_note_ids (void* data, int num_columns, char** row_fields, char** col_names)
{
	// :TODO:
	
	// DB_Access* db_access = reinterpret_cast<DB_Access*>(data);
	// map<int, string> values {};
	// for (int i = 0; i < num_columns; i++) {
	// 	if (row_fields[i]) {
	// 		values[i] = string(row_fields[i]);
	// 	}
	// 	else {
	// 		values[i] = string("");
	// 	}
	// }
	// Note note = Note (values);
	// Omni_DB& omni_db {db_access->omni_db};
	// omni_db.notes_tab.insert(note);
	LOG(lDBG) << "delete note callback " << endl;
	return 0;
}

bool
DB_Access::delete_note_ids (std::set<db_timestamp_t> ids)
{
	// :TODO: Assumes db is openend correctly already
	//
	// :TODO: Doesn't check whether the ids really exist in the database since
	// the calling entity has retrieved those directly.  Yet, file *consistency*
	// could be an issue, still

	// Approach: Delete row by row
	bool res = true;
	string cmd_pre {"DELETE FROM notes WHERE"};
	for (auto id: ids) {
		string id_str = std::to_string(id);
		string cmd_cond = " creation = '" + id_str + "';";
		string cmd {cmd_pre + cmd_cond};

		auto callback = static_cast<sqlite3_callback>(callback_delete_note_ids);

		int rc = sqlite3_exec(db_handle_, cmd.c_str(), callback, (void*)this, NULL);
		if (rc != SQLITE_OK) {
			LOG(lERR) << "Deleting entry " << id_str
					  << " from notes table failed!" << endl;
			res = false;
		}
		else {
			// :TODO: Remove entry from the table set
			LOG(lINFO) << "SUCCESS: Deleting note entry: " << id_str << endl;
		}
	}
	return res;
}

std::array<Note::Fields, Note::fields_size> Note::fields_keys_array {create_fields_array_()};
