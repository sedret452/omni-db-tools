PROGS=merge-omni-db inspect-omni-db
MODULES=omni-db omni-db-common omni-db-algo utils
MODULES_OBJS=$(foreach module, $(MODULES), $(addprefix $(module), .o))

CXX=g++-10
CXXFLAGS=-std=c++17 -O0 -Wall -Wextra $(shell pkg-config --cflags libcrypto sqlite3)
LDLIBS=$(shell pkg-config --libs libcrypto sqlite3)

# Manually collect all relevant source code into dedicated variables
# (usable for cscope)
SRC_PROGS = $(foreach prog, $(PROGS), $(addprefix $(prog), .cc))
SRC_MODULES = $(foreach module, $(MODULES), $(addprefix $(module), .cc .hh))
SRC_OTHER = logcpp.hh

# Make each target also dependent on Makefile
.EXTRA_PREREQS = Makefile

all: $(PROGS) $(MODULES_OBJS)

merge-omni-db: merge-omni-db.cc $(MODULES_OBJS)
	$(CXX) $(CXXFLAGS) -o merge-omni-db $^ $(LDLIBS)
merge-omni-db: .EXTRA_PREREQS = logcpp.hh

inspect-omni-db: inspect-omni-db.cc $(MODULES_OBJS)
	$(CXX) $(CXXFLAGS) -o inspect-omni-db $^ $(LDLIBS)
inspect-omni-db: .EXTRA_PREREQS = logcpp.hh

omni-db-common.o: omni-db-common.cc omni-db-common.hh utils.o
omni-db-common.o: .EXTRA_PREREQS = logcpp.hh

omni-db.o: omni-db.cc omni-db.hh omni-db-common.o utils.o 
omni-db.o: .EXTRA_PREREQS = logcpp.hh

omni-db-algo.o: omni-db-algo.cc omni-db-algo.hh omni-db.o omni-db-common.o utils.o
omni-db-algo.o: .EXTRA_PREREQS = logcpp.hh

utils.o: utils.cc utils.hh
utils.o: .EXTRA_PREREQS = logcpp.hh

.PHONY: clean cscope cscope-file-db clean-cscope clean-cscope-file-db

clean:
	@echo "Cleaning up"
	@for f in $(PROGS) $(MODULES_OBJS) ; do if [ -e "$$f" ] ; then rm $$f ; fi ; done

# Note: the dependency to "cscope-file-db" is left out on purpose,
#       to allow only for manual cscope.files generation
cscope: $(SRC_PROGS) $(SRC_MODULES) $(SRC_OTHER)
	cscope -bq -k -I/usr/include -I/usr/include/c++/10

clean-cscope:
	@for f in  $(wildcard $(addprefix cscope,.in.out .po.out .out)) ; do rm $$f ; done

cscope-file-db:
	@echo "Creating cscope.files"
	@{ for f in ${SRC_PROGS} ${SRC_MODULES} ${SRC_OTHER} ; do echo $$f; done } | tee cscope.files


clean-cscope-file-db:
	@for f in $(wildcard cscope.files) ; do rm $$f ; done
