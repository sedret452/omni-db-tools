/* SPDX-License-Identifier: GPL-2.0-only */

/* omni-db.hh: Omni Notes DB classes
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

#ifndef __OMNI_DB_HH__
#define __OMNI_DB_HH__

extern "C" {
#include <sqlite3.h>
}

#include <string>
#include <set>
#include <map>
#include <stdexcept>
#include <array>

#include "utils.hh"
#include "omni-db-common.hh"

using std::string, std::ostream;
using std::map, std::set;

// :TODO: Should only work, if create_checksum() is available
//
// Possible Approaches:
//   https://stackoverflow.com/questions/41936763/type-traits-to-check-if-class-has-member-function
//   https://codereview.stackexchange.com/questions/142655/c-template-type-traits-for-class-members
//
// template<class T>
// using has_create_checksum =
// 	decltype(std::declval<T&>().create_checksum(void));
// static_assert(std::experimental::is_detected_exact_v<string, has_create_checksum, Note>);
//
// template<class C>
// class checksum_class : public C {
// 	string checksum;
// };

// format detail level for object strings (to_string(), ...)
// 
// "pretty"   -> well formatted but may not provide full detail
// "detailed" -> (well) formatted but with full detail level
// "raw"      -> plain stringification without additional format symbols
// "id_title" -> very short representation with ID and Title only
enum class str_fmt { pretty, detailed, raw, id_title, creation_date_title };

class DB_Algo;

/* C++ representation of a "notes" table. */
class Note {
	db_timestamp_t creation; // Primary Key
	db_timestamp_t last_modification;
	db_text_t title;
	db_text_t content;
	db_flag_t archived;
	db_flag_t trashed;
	db_timestamp_t alarm {0};
	db_flag_t reminder_fired;
	db_text_t recurrence_rule; // ??
	db_degree_t latitude;
	db_degree_t longitude;
	db_text_t address;
	// Category is a user-defined data type with an `id` member (of Java type
	// "Long"), hence `category_id` just references to it.
	db_category_id_t category_id {0};
	db_flag_t locked;
	db_flag_t checklist;

	// extra field
	string checksum_;

public:
	enum class Fields {
		creation = 0, // Primary Key
		last_modification = 1,
		title = 2,
		content = 3,
		archived = 4,
		trashed = 5,
		alarm = 6,
		reminder_fired = 7,
		recurrence_rule = 8,
		latitude = 9,
		longitude = 10,
		address = 11,
		category_id = 12,
		locked = 13,
		checklist = 14,
	};
	static const uint8_t fields_size = 15;
	static const std::array<std::string, fields_size> fields_names;
	
	using Fields_Diff = std::array<bool, fields_size>;

	bool        equals_id      (const db_timestamp_t& id) const;

	bool        is_same_as     (const Note& note) const;
	bool        has_equal_data (const Note& note, const Fields f);
	Fields_Diff has_equal_data (const Note& note);
	
	bool        is_equal_all (const Note& note);
	bool        is_equal_ign_last_modification (const Note& note);

	string to_string (str_fmt=str_fmt::pretty) const;

	// Checksum (all fields as string)
	string checksum (void);

	
	// Checksum of core data (creation, title, content, checklist flag)
	string chksum_core_data (void) const;
	
	// Checksum of base metadata
	// (alarm, recurrence_rule, latitude, longitude, address, category_id)n
	// Note: arguably "modification" is not part here
	string chksum_base_metadata (void) const;
	
    // Checksum of all metadata
	// ("base metadata" + last_modification, archived, trashed, reminder_fired,
	// locked)
	string chksum_full_metadata (void) const;

	// Checksum of all fields but ignore last_modification
	string chksum_ign_last_modification (void) const;
	
	// One checksum to rule them all
	string chksum_data (void); // Core + base metadata
	string chksum_data_full (void); // Core & full metadata

	Note() { };
	Note(map<int, string> values);

	// Note: Specializations in .cc file
	template <Fields field>
	struct Field_Type {
		using value_type = nullptr_t;
		static nullptr_t Note::* mem_ptr;
		// using value_type = db_text_t;
		// static db_text_t Note::* mem_ptr;// {&Note::title};
	};

	template<Note::Fields field>
	static std::string
	field_to_string (Note n)
	{
		using field_type = Note::Field_Type<field>;
		constexpr auto mem_p {field_type::mem_ptr};

		std::stringstream ss;
		ss << (n.*mem_p);
	
		return ss.str() ;
	}

	// :TODO: Should be private
	template <int N>
	static constexpr void 
	static_init_fields_array_(std::array<Note::Fields, Note::fields_size> &array)
	{
		static_assert(N >= 0 and N <= Note::fields_size,
					  "Provided integer parameter out of bounds!");

		array[N] = static_cast<Note::Fields>(N);	
		if constexpr (N > 0) {
			static_init_fields_array_<N-1>(array);
		}
	}

	// :TODO: Should be private
	static constexpr std::array<Fields, fields_size>
	create_fields_array_ ()
	{
		std::array<Fields, fields_size>&& result {};
		static_init_fields_array_<fields_size - 1> (result);
		return result;
	}

	/* Static array that holds all possible keys of Note::Fields.  This can be
	   used to "iterate" over all keyes of Note::Fields. To be able to use this,
	   make sure to prepare the array in the following way:
 
 	   // Note: another way of initiaizing an array for this but using integers
	   //       instead (expecting the underlying type to be int-compatible):
	   //
	   // std::array<int, Note::fields_size> index_list;
	   // std::iota (std::begin(index_list), std::end(index_list), 0);
	*/
	static std::array<Fields, fields_size> fields_keys_array; // :TODO: Should be const

	friend bool operator< (const Note& l, const Note& r);
	friend ostream& operator<< (ostream&, const Note&);

	friend DB_Algo;
};

template <>
struct Note::Field_Type<Note::Fields::creation> {
	using value_type = db_timestamp_t;
	constexpr static value_type Note::* mem_ptr {&Note::creation};
};
template <>
struct Note::Field_Type<Note::Fields::last_modification> {
	using value_type = db_timestamp_t;
	constexpr static value_type Note::* mem_ptr {&Note::last_modification};
};
template <>
struct Note::Field_Type<Note::Fields::title> {
	using value_type = db_text_t;
	constexpr static value_type Note::* mem_ptr {&Note::title};
};
template <>
struct Note::Field_Type<Note::Fields::content> {
	using value_type = db_text_t;
	constexpr static value_type Note::* mem_ptr {&Note::content};
};
template <>
struct Note::Field_Type<Note::Fields::archived> {
	using value_type = db_flag_t;
	constexpr static value_type Note::* mem_ptr {&Note::archived};
};
template <>
struct Note::Field_Type<Note::Fields::trashed> {
	using value_type = db_flag_t;
	constexpr static value_type Note::* mem_ptr {&Note::trashed};
};
template <>
struct Note::Field_Type<Note::Fields::alarm> {
	using value_type = db_timestamp_t;
	constexpr static value_type Note::* mem_ptr {&Note::alarm};
};
template <>
struct Note::Field_Type<Note::Fields::reminder_fired> {
	using value_type = db_flag_t;
	constexpr static value_type Note::* mem_ptr {&Note::reminder_fired};
};
template <>
struct Note::Field_Type<Note::Fields::recurrence_rule> {
	using value_type = db_text_t;
	constexpr static value_type Note::* mem_ptr {&Note::recurrence_rule};
};
template <>
struct Note::Field_Type<Note::Fields::latitude> {
	using value_type = db_degree_t;
	constexpr static value_type Note::* mem_ptr {&Note::latitude};
};
template <>
struct Note::Field_Type<Note::Fields::longitude> {
	using value_type = db_degree_t;
	constexpr static value_type Note::* mem_ptr {&Note::longitude};
};
template <>
struct Note::Field_Type<Note::Fields::address> {
	using value_type = db_text_t;
	constexpr static value_type Note::* mem_ptr {&Note::address};
};
template <>
struct Note::Field_Type<Note::Fields::category_id> {
	using value_type = db_category_id_t;
	constexpr static value_type Note::* mem_ptr {&Note::category_id};
};
template <>
struct Note::Field_Type<Note::Fields::locked> {
	using value_type = db_flag_t;
	constexpr static value_type Note::* mem_ptr {&Note::locked};
};
template <>
struct Note::Field_Type<Note::Fields::checklist> {
	using value_type = db_flag_t;
	constexpr static value_type Note::* mem_ptr {&Note::checklist};
};


/* Omni_DB: Specify the (logical) structure of the DB and provides it as a proxy
 * object. Provided operations extend the functionality of the plain DB data, by
 * providing additional checksum calculation, stringyfication, as also data
 * property/invariant calculations.
 */
class Omni_DB {
	string version {"5.5.2"}; // DB version of "Omni Notes"
	string notes_tab_to_string () const;

public:
	set<Note> notes_tab {};
	string checksum_notes_tab  () const;
	size_t size_notes_tab () const;

	Omni_DB ();
	Omni_DB (Omni_DB&& old);
	Omni_DB& operator= (Omni_DB&&);

	// std::set<Note>::const_iterator get_node_by_id(db_timestamp_t id) const;
	// using Note_Const_It = std::set<Note>::const_iterator;
	const Note* get_node_by_id(db_timestamp_t id) const;
	// Note_Const_It get_node_by_id_ng(db_timestamp_t id) const;

};

/* DB_File: Provides all operations that strictly relate only to file handling
 * and not to the actual DB content. This allows to abstract OS
 * environment-specific details of DB file handling.
 *
 * Please note: While all main operations of this class will be accessed by the
 * user through DB_Access class, object creation must be handled by the user
 * directly.
 */
class DB_File {

public:
	string filename;
	
	DB_File () = delete;
	DB_File (string filename);
	DB_File (DB_File&&) = default;
	DB_File (const DB_File&) = delete;
	~DB_File () = default;
	DB_File& operator= (DB_File&&) = default;
	bool is_accessible ();
	// I would like to use move semantics here, but it keeps fucking
	// up things
	DB_File copy_to (string to_filename); // :TODO: should throw error

	bool remove ();	
};

class DB_Access_Err : public std::runtime_error {
public:
	DB_Access_Err() : std::runtime_error("DB access error") { }
};

/* DB_Access: Encapsulates all DB operations into a single class.
 *
 * The details of the internal DB structure are encapsulated through Omni_DB
 * class, which gives more like a logical view on the DB, whereas DB_File class
 * encapsulates the details of DB file handling.
 */
class DB_Access {
	DB_File db_file_;
	sqlite3* db_handle_ {nullptr};

	bool is_empty ();
public:
	Omni_DB omni_db {};

	DB_Access () = delete;
	DB_Access (string filename); // throws error
	DB_Access (DB_File&& db_file); // throws error
	DB_Access (DB_Access&&);
	DB_Access (const DB_Access&) = delete;
	~DB_Access ();
	DB_Access& operator= (DB_Access&&);

	bool open_db_file ();
	void close_db_file ();
	bool close_and_remove_db_file ();

	bool read_notes_table ();
	bool debug_notes_table ();
	bool merge_notes_table (string);

	bool delete_note_ids (std::set<db_timestamp_t> ids);

	bool sync_from_notes_table (); /* :TODO: same functionality than current read.. method */
	bool sync_into_notes_table (); /* :TODO: Implement*/

	Omni_DB&& create_Omni_DB (void);

	friend DB_Algo;
};

#endif //__OMNI_DB_HH__
