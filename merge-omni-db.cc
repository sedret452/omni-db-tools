/* SPDX-License-Identifier: GPL-2.0-only */

/*
 * merge-omni-db.cc: Merge "Omni Notes" (FOSS) Databases
 *
 * (part of omni-db-tools)
 * Copyright (C) 2022 sedret452
 */

extern "C" {
#include <unistd.h> // for getopt
#include <getopt.h> // for getopt long
}

#include <iostream>
#include <string>
#include <filesystem>
#include <vector>

#ifndef LOG_LEVEL
#define LOG_LEVEL lINFO
#endif
#include "logcpp.hh"

#include "omni-db.hh"
#include "omni-db-algo.hh"

using std::cout, std::cerr, std::endl;
using std::string;
using std::map, std::vector;

const char* prog_name {"merge-omni-db"};
const char* prog_version_str {"0.7"};

void
print_version ()
{	
	LOG(lSTD) << ::prog_name << ", version " << ::prog_version_str << endl;	
}

void
print_usage ()
{
	LOG(lSTD)
		<< ::prog_name << ", version " << ::prog_version_str << endl
		<< "Usage: " << endl
		<< "  " << ::prog_name << " [--help|--version] [Algorithm] [-o out -i in1 -i in2 ...] " << endl
		<< endl
		<< "  General options" << endl
		<< "    -h, --help      Show this help" << endl
		<< "    -v, --version   Show version" << endl
		<< endl
		<< "  Mode syntax:" << endl
		<< "    -o <out-db>  Select the out DB, must be provided exactly once" << endl
		<< "    -i <in-db>   Select an in DB, at minimum two DBs must be provided" << endl
		<< endl
		<< "  Merge Algorithms" << endl
		<< "    -D | --merge-disjoint-only\t(default)" << endl
		<< "      Each input DB is assumed to be disjoint to all other DBs." << endl
		<< "    -S | --merge-safe-intersect" << endl
		<< "      All intersecting elements can be safely merged (i.e. same data)." << endl
		;
}

// Wrapping CLI error codes into an enumeration for readability.
// (note: Class enums turned out to be impractical for this, due to
//  additionally required value translation.)
enum cli_err {
	ok = 0, // No error, all is fine
	unknown_option = 1,
	multiple_modes_mixed = 2,
	too_many_outputs = 3,
	output_unset = 4,
	min_input_files = 5,
	not_exact_file_count = 6
};

// Check precondition for --merge-.. modes
// Returns error codes: 0 -> No error
int
merge_db_precond_err (const string out_filename, const vector<string>& in_filename_v)
{
	// Output filename must not be empty (unset)
	if (out_filename.empty()) {
		LOG(lERR) << "Merge-to output file not set!" << endl;
		return (cli_err::output_unset);
	}
	// At least two input files need to be provided	
	else if (in_filename_v.size() < 2) {
		LOG(lERR) << "At least two Merge-from input files need to be provided!" << endl;
		return (cli_err::min_input_files);
	}
	else {
		return 0;
	}
}

// :TODO: Way too hacky hence should be cleaned up, a little
void
merge_disjoint_only (string out_filename, vector<string>&& in_filename_v)
{
	// (1) files usable (exist and accessible)?
	
	// (2) *Creat backups of source files* and check if backup files
	// were created correctly

	// (3) Copy in_1 file to target_db and check if successful
	// creates out DBs and open it already	
	auto in_1_file {DB_File(in_filename_v[0])};
	LOG(lINFO) << "Merging (by copy) from <" << in_1_file.filename << ">" << endl;	
	auto out_file  {std::move(in_1_file.copy_to(out_filename))};
	DB_Access out_dba  {DB_Access(std::move(out_file))};
	{
		out_dba.read_notes_table();
		auto md5  {out_dba.omni_db.checksum_notes_tab()};
		auto size {out_dba.omni_db.size_notes_tab()};
		LOG(lINFO) << "db (out): <" << out_filename << "> size[" << size << "], md5[" << md5 << "]" << endl;
	}

	// (4) Merge source files into target file by using SQL attach
	// strategy (need to check for correct CWD! or pathname)
	// string in_2_name {in_filename_v[1]};
	// out_dba.merge_notes_table (in_2_name);
	for (auto it{in_filename_v.begin() + 1}; it != in_filename_v.end(); it++)
	{
		string in_2_name {*it};
		LOG(lINFO) << "Merging from <" << in_2_name << ">" << endl;
		out_dba.merge_notes_table (in_2_name);
		out_dba.read_notes_table();
		auto md5  {out_dba.omni_db.checksum_notes_tab()};
		auto size {out_dba.omni_db.size_notes_tab()};
		LOG(lINFO) << "db (out): <" << out_filename << "> size[" << size << "], md5[" << md5 << "]" << endl;
	}

	// (5) Additional run for debugging only
	{
		out_dba.read_notes_table();
		auto md5  {out_dba.omni_db.checksum_notes_tab()};
		auto size {out_dba.omni_db.size_notes_tab()};
		LOG(lINFO) << "db (out): <" << out_filename << "> size[" << size << "], md5[" << md5 << "]" << endl;
	}
}

// Merge given intput file names into given DB Access handle.  For any merge
// following conditions are fulfilled:
//   Cond A: to-be-merged DBs are completely disjoint, or
//
//   Cond B: intersected elements are duplicates only, i.e. they represent the
//           same value (ignoring 'last_modification' field).
void
merge_safe_intersect_rest (DB_Access& dba1, vector<string>&& rest)
{
	string tmp_name {"tmp_.sql"}; // :TODO: Use proper temporary name creation

	// Merge the rest input files into dba1, one-by-one
	for (auto it{rest.begin()}; it != rest.end(); it++) {
		string next_name {*it};
		auto tmp_file {DB_File(next_name).copy_to(tmp_name)};
		DB_Access tmp_dba {DB_Access(std::move(tmp_file))};
		tmp_dba.read_notes_table();

		// Estimate which merge condition is fulfilled
		LOG(lINFO) << "Checking disjoint <" << next_name << ">" << endl;
		auto intersect_ids = DB_Algo::intersect_ids (dba1, tmp_dba);
		bool dbs_are_fully_disjoint = (std::size(intersect_ids) == 0);
		if (dbs_are_fully_disjoint) {
			// Merge by using simple attach approach
			dba1.merge_notes_table (tmp_name);
			LOG(lINFO) << "Merging (attach) from <" << next_name << ">" << endl;
		}
		else {
			// (0) :TODO: (Ignoring last_modification), check if intersection
			// elements represent the same value
			
			// (1) Delete intersect nodes from tmp_dba
			tmp_dba.delete_note_ids (intersect_ids);

			// (2) Use regular attach strategy to merge the now fully disjoint
			// tables
			dba1.merge_notes_table (tmp_name);

			// :TODO: Current merging strategy ignores to set most recent
			// 'last_modification' date, an adjustment to the most recent date
			// should be prepared before the actual merge.
		}
		tmp_dba.close_and_remove_db_file();
	}	
}

void
merge_safe_intersect (string out_filename, vector<string>&& in_filename_v)
{
	// (1) Create output file by simply copying from the first input file.
	auto in_1_file {DB_File(in_filename_v[0])};
	LOG(lINFO) << "Merging (by copy) from <" << in_1_file.filename << ">" << endl;	
	auto out_db_file  {std::move(in_1_file.copy_to(out_filename))};
	DB_Access out_dba  {DB_Access(std::move(out_db_file))};
	{
		out_dba.read_notes_table();
		auto md5  {out_dba.omni_db.checksum_notes_tab()};
		auto size {out_dba.omni_db.size_notes_tab()};
		LOG(lINFO) << "db (out): <" << out_filename << "> size[" << size << "], md5[" << md5 << "]" << endl;
	}

	// (2) Merge the rest of the input files into output file by using improved
	// merge strategy.
	in_filename_v.erase(std::begin(in_filename_v)); // :TODO: could be improved
	merge_safe_intersect_rest (out_dba, std::move(in_filename_v));
}

int
main (int argc, char** argv)
{
	// Note: Most dev modes are moved into a separate program for now. Since
	// there is the plan to introduce heuristics into *this* program, leaving
	// the cli_mode enum may come in handy again. Let's wait and see
	// (2022-08-04).
	enum class cli_mode { 
		merge_disjoint_only
		, merge_safe_intersect
		, unset_
	};
	cli_mode cli {cli_mode::merge_disjoint_only}; // Merge default = --disjoint-dbs-only
	bool cli_mode_set_by_user {false};
		
	vector<string> in_db_filenames {};
	string out_db_filename {};

	// Use compact names to reduce line length in opts definition
	#define ARG_N no_argument
	#define ARG_Y required_argument
	#define ARG_O optional_argument
	
	static char short_opts[] {"+hi:o:v"};
	static struct option long_opts[] = {
		{"help",                 ARG_N, 0, 'h'},
		{"in-db",                ARG_Y, 0, 'i'},
		{"merge-disjoint-only",  ARG_N, 0, 'D'},
		{"merge-safe-intersect", ARG_N, 0, 'S'},
		{"out-db",               ARG_Y, 0, 'o'},
		{"version",              ARG_N, 0, 'v'},
		{0, 0, 0, 0}
	};
	
	::opterr = 0;		
	int opt_index {0};
	bool getopt_done {false};
	while (not getopt_done) {
		int curind {optind}; // See: https://stackoverflow.com/a/2725408
		int c {getopt_long (argc, argv, short_opts, long_opts, &opt_index)};
		switch (c) {
		    case -1:
				getopt_done = true;
				break;
				
			case 'h': // --help
				print_usage();
				exit (cli_err::ok);
				break;

		    case 'D': // --merge-disjoint-only
				if (cli_mode_set_by_user && cli != cli_mode::merge_disjoint_only) {
					cerr << "Error: --merge-disjoint-only, another merge mode is set already!" << endl;
					exit (cli_err::multiple_modes_mixed);
				}
				cli = cli_mode::merge_disjoint_only;
				cli_mode_set_by_user = true;
				break;
				
			case 'i': // --in-db
				{
					in_db_filenames.emplace_back (string(optarg));
				}
				break;

			case 'o': // --out-db
				{
					if (out_db_filename.empty()) {
						out_db_filename = (string(optarg));
					}
					else {
						cerr << "Error: -o only selectable once!" << endl;
						exit (cli_err::too_many_outputs);
					}
				}
				break;

		    case 'S': // --merge-safe-intersect
				if (cli_mode_set_by_user && cli != cli_mode::merge_safe_intersect) {
					cerr << "Error: --merge-safe-intersect, another merge mode is set already!" << endl;
					exit (cli_err::multiple_modes_mixed);
				}
				cli = cli_mode::merge_safe_intersect;
				cli_mode_set_by_user = true;
				break;
				
			case 'v': // --version
				print_version();
				exit(cli_err::ok);
				break;

		    case '?': // Handling unknown options
				if (optopt) {
					if (isprint (optopt)) {
						cerr << "Unknown short option: -" << (char) optopt << endl;
					}
					else {
						cerr << "Unknown short option: UNPRINTABLE" << endl;
					}
				}
				else {
					cerr << "Unknown long option: " << argv[curind] << endl;
				}
				print_usage();
				exit (cli_err::unknown_option);
		}
	}

	LOG(lINFO) << ::prog_name << endl;
	switch (cli) {
	case cli_mode::merge_disjoint_only:
		if (int err_code = merge_db_precond_err (out_db_filename, in_db_filenames))
			exit (err_code);
		else
			merge_disjoint_only (out_db_filename, std::move(in_db_filenames));
		break;
		
	case cli_mode::merge_safe_intersect:
		if (int err_code = merge_db_precond_err (out_db_filename, in_db_filenames))
			exit (err_code);
		else
			merge_safe_intersect (out_db_filename, std::move(in_db_filenames));
		break;
		
	case cli_mode::unset_:
		LOG(lERR) << "[INTERNAL] No mode selected!" << endl;
		break;
	}
	return (cli_err::ok);
}
